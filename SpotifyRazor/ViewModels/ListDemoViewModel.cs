﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortableRazor;
using PortableRazor.Web.Binding;

namespace SpotifyRazor.ViewModels
{
    public abstract class ListDemoViewModel : ViewModelBase
    {
        public class Album
        {
            public string Artist;
            public string AlbumTitle;
            public int Year;

            public override string ToString()
            {
                return $"{Artist} - {AlbumTitle} ({Year})";
            }
        }

        public enum RecordType
        {
            Album,
            Single,
            Compilation
        }

        protected Bindable<string> ArtistToAdd = new Bindable<string>("Arcade Fire");
        protected Bindable<string> AlbumTitleToAdd = new Bindable<string>("Scenes from the Suburbs");
        protected Bindable<int> YearToAdd = new Bindable<int>(2011);

        protected BindableList<Album> AlbumList = new BindableList<Album>();

        /// <summary>
        /// Used for the Selection.
        /// </summary>
        protected BindableSelection<RecordType> TypeOfRecord = BindableSelection.FromEnum<RecordType>();

        /// <summary>
        /// Computed bindable to illustrate the usage in the ViewModel code.
        /// </summary>
        protected BindableComputed<string> CombinedToAdd = new BindableComputed<string>();

        protected ListDemoViewModel()
        {
            // subscribe other bindables to know when they have been changed
            CombinedToAdd.Subscribe(ArtistToAdd, AlbumTitleToAdd, YearToAdd);

            // set the function to evaluate when one of the dependencies changed
            CombinedToAdd.SetFunction(() => $"{ArtistToAdd} - {AlbumTitleToAdd} ({YearToAdd})");
        }

        public override void Dispose()
        {
            ArtistToAdd.Dispose();
            AlbumTitleToAdd.Dispose();
            YearToAdd.Dispose();
            AlbumList.Dispose();
            TypeOfRecord.Dispose();
            CombinedToAdd.Dispose();

            // and always call base.Dispose();
            base.Dispose();
        }

        protected override void Initialize()
        {
            AlbumList.Add(new Album() {AlbumTitle = "Reflector", Artist = "Arcade Fire", Year = 2013});
            AlbumList.Add(new Album() {AlbumTitle = "The Suburbs", Artist = "Arcade Fire", Year = 2010});
        }

        protected void AddAlbum()
        {
            // you could also do this in the event handler on the Razor side
            AlbumList.Add(new Album() { AlbumTitle = AlbumTitleToAdd.Value, Artist = ArtistToAdd.Value, Year = YearToAdd.Value });
        }
    }
}
