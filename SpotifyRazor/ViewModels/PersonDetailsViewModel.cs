﻿using PortableRazor;
using PortableRazor.Web.Binding;
using PortableRazor.Web.Mvvm;

namespace SpotifyRazor.ViewModels
{
    public abstract class PersonDetailsViewModel : ViewModelBase<string, int>
    {
        protected Bindable<string> Name = new Bindable<string>();
        protected Bindable<int> Age = new Bindable<int>(0);
        protected Bindable<FavoriteMovie> Movie = new Bindable<FavoriteMovie>();

        protected override void Initialize(string name, int age)
        {
            Name.Value = name; // is the name while the parent page was loaded .. not the actual one
            Age.Value = age;    // same here.. need to handle bindables differently
                                                // dynamic is not cool.. looking for something more strongly typed

            // not working at the moment.. passing (nested) bindables is a problem
            Movie.Value = new FavoriteMovie();
           // Movie.Value = modelParameters.Movie.Value;
        }

        public void GoBack()
        {
            Navigation.Back();
        }

        /// <summary>
        /// Always free the ViewModel and your Bindables!
        /// </summary>
        public override void Dispose()
        {
            Name.Dispose();
            Age.Dispose();

            Movie.Value?.Dispose(); // tricky..
            Movie.Dispose();

            // and always call base.Dispose();
            base.Dispose();
        }
    }
}
