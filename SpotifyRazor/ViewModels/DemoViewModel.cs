﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PortableRazor;
using PortableRazor.Web.Binding;

namespace SpotifyRazor.ViewModels
{
    /*
    public class BindingModelNode : DynamicObject
    {
        // The inner _ChildNodes.
        private readonly Dictionary<string, BindingModelNode> _ChildNodes = new Dictionary<string, BindingModelNode>();
        private BindingModelNode _Parent = null;

        // If you try to get a value of a property 
        // not defined in the class, this method is called.
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            // Converting the property name to lowercase
            // so that property names become case-insensitive.
            string name = binder.Name.ToLower();

            // If the property name is found in a _ChildNodes,
            // set the result parameter to the property value and return true.
            // Otherwise, return false.
            BindingModelNode child = null;
            bool success = _ChildNodes.TryGetValue(name, out child);
            result = child;
            return success;
        }

        // If you try to set a value of a property that is
        // not defined in the class, this method is called.
        public override bool TrySetMember(
            SetMemberBinder binder, object value)
        {
            // Converting the property name to lowercase
            // so that property names become case-insensitive.
            BindingModelNode node = value as BindingModelNode;
            if (node == null) return false;

            // avoid circles
            if (node._Parent != null)
            {
                Debug.WriteLine("[BindingModelNode] Node already in the tree.");
                return false;
            } 

            _ChildNodes[binder.Name.ToLower()] = node;
            node._Parent = this;
            return true;
        }
    }*/

    public abstract class DemoViewModel : ViewModelBase<String>
    {
        public Bindable<string> Query = new Bindable<string>("");
        public Bindable<int> Number = new Bindable<int>(42);
        public Bindable<bool> IsEnabled = new Bindable<bool>(true);
        public Bindable<float> ElapsedTime = new Bindable<float>(0.0f);

        public string[] DemoList = {"None", "Option 1", "Option 2", "Alternative"};

        /// <summary>
        /// Always free the ViewModel and your Bindables!
        /// </summary>
        public override void Dispose()
        {
            Query.Dispose();
            Number.Dispose();
            IsEnabled.Dispose();
            ElapsedTime.Dispose();

            // and always call base.Dispose();
            base.Dispose();
        }

        protected override void Initialize()
        {
            Initialize("Vla");
        }

        protected override void Initialize(string query)
        {
            Query.Value = query;
            //RunUpdatesAsync();
        }

        private async void RunUpdatesAsync()
        {
            // this will update at 30 Hz
            while (true)
            {
                await Task.Delay(32);
                ElapsedTime.Value += 0.032f;
            }
        }

        /// <summary>
        /// A method that is called after pressing a button
        /// </summary>
        public void Foo(String name, int age)
        {

        }
    }
}
