﻿using System;
using PortableRazor;
using PortableRazor.Web.Binding;
using PortableRazor.Web.Mvvm;

namespace SpotifyRazor.ViewModels
{
    delegate void ParamsDelegate(params object[] args);

    public class FavoriteMovie : IDisposable
    {
        public string Name = "The Shawshank Redemption";

        // if you do that, make sure you are able to dispose it 
        // here is hard to keep track of 'FavoriteMovie' instances (see PersonViewModel::Dispose())
        public Bindable<int> Year = new Bindable<int>(1994);

        public void Dispose()
        {
            Year.Dispose();
        }
    }

    public struct PersonModelParameters
    {
        public string Name;
        public int Age;
    }

    public abstract class PersonViewModel : ViewModelBase<PersonModelParameters>
    {
        protected Bindable<string> Name = new Bindable<string>();
        protected Bindable<int> Age = new Bindable<int>();
        protected Bindable<FavoriteMovie> Movie = new Bindable<FavoriteMovie>();
        protected Bindable<string> Output = new Bindable<string>();


        /// <summary>
        /// OPTIONAL:
        /// (parameterless) initialization.
        /// You need to override this, if you want to navigate without parameters. 
        /// </summary>
        protected override void Initialize()
        {
            Initialize(new PersonModelParameters() {Name = "<Nobody>", Age = 1337});
        }

        /// <summary>
        /// Default initialization to setup your view model.
        /// Parameters are defined by the generic base type.
        /// </summary>
        /// <param name="modelParameters"></param>
        protected override void Initialize(PersonModelParameters modelParameters)
        {
            // initialize the bindables in their constructor or setting their Value parameter
            Name.Value = modelParameters.Name;
            Age.Value = modelParameters.Age;
            Movie.Value = new FavoriteMovie();
        }

        /// <summary>
        /// Always free the ViewModel and your Bindables!
        /// </summary>
        public override void Dispose()
        {
            Name.Dispose();
            Age.Dispose();
            Movie.Value?.Dispose(); // since the value is also a disposable, but we usually don't want to call 'dispose' automatically every time the value changes.
            Movie.Dispose();
            Output.Dispose();

            // and always call base.Dispose();
            base.Dispose();
        }


        protected void ChangeAge(string name, int age, bool add)
        {
            // Change bindables by changing their value property
            Age.Value = age + (add ? 1 : -1);
        }

        protected void TellMovie(string name, FavoriteMovie movie)
        {
            Output.Value = $"The favorite movie of {name} is {movie.Name} from {movie.Year}.";
        }

        protected void AcceptEverything(dynamic anything)
        {
            Output.Value = anything.ToString();
        }

        protected void ShowDetails()
        {
            // Navigation from code
            // organized as pipeline:
            // - select the view navigate to by:    Navigation.To<SpotifyRazor.Views.Mvvm.Demos.PersonDetails>()
            // - pass parameters by:                .With(Name, Age)
            // - save navigation history by:        .NoHistory()
            // - start navigation by:               .Go()
            Navigation.To<SpotifyRazor.Views.Mvvm.Demos.PersonDetails>().With(Name, Age).NoHistory().Go();
        }
    }
}
