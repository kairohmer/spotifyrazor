﻿using System;
using System.Globalization;
using PortableRazor;
using SpotifyRazor.Controllers;
using SpotifyRazor.Controllers.Mvc;
using SpotifyRazor.Controllers.Mvvm;
using SpotifySharp;

namespace SpotifyRazor
{
    public class SpotifyRazorAppContext : ApplicationContext
    {
        public readonly SpotifyRazorApp App;

        public SpotifyRazorAppContext(SpotifyRazorApp app)
        {
            App = app;
        }
    }

    public class SpotifyRazorApp
    {
        public readonly Spotify SpotifyApi;
        public readonly PortableRazorContext Context;

        public Action Run = null; 

        public SpotifyRazorApp(PortableRazorContext context)
        {
            // setup localization (kind of disable it)
            // if you change this keep in mind to also check for correct number formating
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;

            // razor context
            Context = context;

            // connect to spotify
            SpotifyApi = new Spotify();

            // create controllers
            // you can pass your application to access you main model.. 
            SpotifyRazorAppContext appContext = new SpotifyRazorAppContext(this); // .. by creating an app context

            // register routes
            Context.Mvc.RegisterController<StartController>(appContext);

            // mvc
            Context.Mvc.RegisterController<SearchController>(appContext);
            Context.Mvc.RegisterController<ArtistController>(appContext);
            Context.Mvc.RegisterController<AlbumController>(appContext, "Mvc.Album");   // you don't need to use the standard route by name convention

            // mvvm (embedded into a mvc)
            Context.Mvc.RegisterController<MainController>(appContext);

            // define the entry point 
            Context.Mvc.SetDefaultController<StartController>(); // by default the first registed one (here just to show)
            Run = () => Context.Run("Index");
        }
    }
}
