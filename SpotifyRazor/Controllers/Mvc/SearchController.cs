﻿using System;
using System.Threading.Tasks;
using PortableRazor;
using PortableRazor.Web.Binding;

namespace SpotifyRazor.Controllers.Mvc
{
    public class SearchController : Controller
    {
        private readonly SpotifyRazorApp m_App;

        public SearchController(ApplicationContext appContext, string name) : base(appContext, name)
        {
            m_App = ((SpotifyRazorAppContext)appContext).App;
        }

        public override void Dispose()
        {
        }

        public void Index()
        {
            // define the view and the layout to show
            View("Index");
        }

        public async void Request(string query, string searchFor)
        {
            if (searchFor == "artist")
            {
                var simplifiedArtists = await m_App.SpotifyApi.SearchArtistAsync(query);
                ViewBag.Model = await m_App.SpotifyApi.GetAsync(simplifiedArtists.Items);
                View("ArtistList");
            }

            else if (searchFor == "album")
            {
                var simplifiedAlbums = await m_App.SpotifyApi.SearchAlbumAsync(query);
                ViewBag.Model = await m_App.SpotifyApi.GetAsync(simplifiedAlbums.Items);
                View("AlbumList");
            }
        }
    }
}
