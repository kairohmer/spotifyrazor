﻿using System;
using PortableRazor;
using SpotifySharp;

namespace SpotifyRazor.Controllers.Mvc
{
    public class AlbumController : Controller
    {
        private readonly SpotifyRazorApp m_App;

        public AlbumController(ApplicationContext appContext, string name) : base(appContext, name)
        {
            m_App = ((SpotifyRazorAppContext)appContext).App;
        }

        public override void Dispose()
        {
        }

        public async void Index(string spotifyID)
        {
            var album = await m_App.SpotifyApi.GetAsync<Album>(spotifyID);
            var tracks = await album.Tracks.GetItemsFromAllPagesAsync();

            ViewBag.Model = new
            {
                Album = album,
                Tracks = tracks // it's not allowed to call await in views (since presentation can not block)
            };

            View(); // uses the view that is named like the current action (name of the method)
        }



    }
}
