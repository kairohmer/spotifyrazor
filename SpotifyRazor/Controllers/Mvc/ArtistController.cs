﻿using PortableRazor;
using SpotifySharp;

namespace SpotifyRazor.Controllers.Mvc
{
    public class ArtistController : Controller
    {
        private readonly SpotifyRazorApp m_App;

        public ArtistController(ApplicationContext appContext, string name) : base(appContext, name)
        {
            m_App = ((SpotifyRazorAppContext)appContext).App;
        }

        public override void Dispose()
        {
        }

        public async void Index(string spotifyID)
        {
            var artist = await m_App.SpotifyApi.GetAsync<Artist>(spotifyID);
            var albumsSimplified = await artist.GetAlbumsAsync(AlbumType.Album);
            var albums = await m_App.SpotifyApi.GetAsync(await albumsSimplified.GetItemsFromAllPagesAsync());

            var singlesSimplified = await artist.GetAlbumsAsync(AlbumType.Single);
            var singles = await m_App.SpotifyApi.GetAsync(await singlesSimplified.GetItemsFromAllPagesAsync());

            ViewBag.Model = new
            {
                Artist = artist,
                Albums = albums,    // it's not allowed to call await in views (since presentation can not block)
                Singles = singles,  //
            };

            View(); // uses the view that is named like the current action (name of the method)
        }
    }
}
