﻿using System;
using System.Threading.Tasks;
using PortableRazor;
using PortableRazor.Web.Binding;

namespace SpotifyRazor.Controllers
{
    public class StartController : Controller
    {
        private readonly SpotifyRazorApp m_App;

        public StartController(ApplicationContext appContext, string name) : base(appContext, name)
        {
            m_App = ((SpotifyRazorAppContext)appContext).App;
        }

        public override void Dispose()
        {
        }

        public void Index()
        {
            // define the view and the layout to show
            View("Index");
        }
    }
}
