﻿using PortableRazor;
using PortableRazor.Web.Mvc;
using SpotifyRazor.ViewModels;

namespace SpotifyRazor.Controllers.Mvvm
{
    public class MainController : Controller
    {
        protected readonly MvcToMvvmBindableViewModelSlot _MainSlot = new MvcToMvvmBindableViewModelSlot();

        private readonly SpotifyRazorApp m_App;

        public MainController(ApplicationContext appContext, string name) : base(appContext, name)
        {
            m_App = ((SpotifyRazorAppContext)appContext).App;
        }

        public override void Dispose()
        {
            _MainSlot.Dispose();
        }

        public void Index()
        {
            // define the view and the layout to show
            ViewBag.Model = new {Slot = _MainSlot};
            View("Index");
        }

        public void ChangeToDemo()
        {
            // show the first view model
            _MainSlot.NavigateTo<SpotifyRazor.Views.Mvvm.Demos.Demo>().With("Peter").Go();
            
        }

        public void ChangeToPerson()
        {
            // you could pass all parameters from razor code as well (see .cshtml)
            _MainSlot.NavigateTo<SpotifyRazor.Views.Mvvm.Demos.Person>().With(new PersonModelParameters() { Name = "Hans", Age = 27 }).NoHistory().Go();
        }
    }
}
