﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using PortableRazor.Andorid;

namespace SpotifyRazor.Android
{
    [Activity(Label = "SpotifyRazor.Android", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        public SpotifyRazorApp App { get; private set; }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // platform web view to display the content
            // there is probably only one which is passed to every controller
            var _WebView = FindViewById<RazorWebView>(Resource.Id._WebView);

            // extracts the embedded data
            // this can be done for multiple assemblies
            var resourceManager = new ResourceManager();
            resourceManager.ExtractResources(
                SpotifyRazor.EmeddedResources.GetAssembly(),
                SpotifyRazor.EmeddedResources.GetEmbeddedContentFolders());

            // create the app
            App = new SpotifyRazorApp(_WebView.RazorContext);

            // add platform components
            App.Context.Register(new HardwareButtonListener());

            // run the app
            App.Run();
        }
    }
}

