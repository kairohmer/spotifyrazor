﻿using Windows.UI.Xaml.Controls;
using PortableRazor.UWP;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SpotifyRazor.UWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public readonly SpotifyRazorApp App;

        public MainPage()
        {
            this.InitializeComponent();

            // extracts the embedded data
            // this can be done for multiple assemblies
            var resourceManager = new ResourceManager();
            resourceManager.ExtractResources(
                SpotifyRazor.EmeddedResources.GetAssembly(),
                SpotifyRazor.EmeddedResources.GetEmbeddedContentFolders());

            // platform web view to display the content
            // there is probably only one which is passed to every controller
            // _WebView;

            // create the app
            App = new SpotifyRazorApp(_WebView.RazorContext);

            // add platform components
            App.Context.Register(new HardwareButtonListener());

            // run the app
            App.Run();
        }
    }
}
