# README #

### What is this repository for? ###

It's used as demo project for my improved PortableRazor library.

### How do I get set up? ###

Clone the repository and checkout master recursively
```bash
git clone <url> --recursive
git submodule foreach --recursive git checkout master
```
Install Visual Studio Extensions 'Razor Generator'

### Contribution guidelines ###

This is experimental. Let's have a chat if you are interested ;)